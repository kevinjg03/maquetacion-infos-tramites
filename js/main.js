$(document).ready(function () {

  // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
  $('.dropdown').on('show.bs.dropdown', function (e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
  });

  // ADD SLIDEUP ANIMATION TO DROPDOWN //
  $('.dropdown').on('hide.bs.dropdown', function (e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
  });

  $('#dropdownjs').click(function (e) {
    var title = $('#dropdownjs:selected').text();
    console.log(title);
    console.log($('#dropdownjs').val());
    $('.dropdown-toggle').html()
  });
});

